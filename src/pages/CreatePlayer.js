import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';

export default function CreatePlayer() {

  return (
    <div className=" p-mx-3">
      <h3>CreatePlayer</h3>
      <div className="p-mt-4">
        <span className="p-float-label">
          <InputText id="in"/>
          <label htmlhtmlFor="in">Username</label>
        </span>
      </div>
      <div className="p-mt-4">
        <span className="p-float-label">
          <InputText id="in"/>
          <label htmlhtmlFor="in">Email</label>
        </span>
      </div>
      <div className="p-mt-4">
        <span className="p-float-label">
          <InputText id="in"/>
          <label htmlhtmlFor="in">Bio</label>
        </span>
      </div>
      <div className="p-mt-4">
        <span className="p-float-label">
          <InputText id="in"/>
          <label htmlhtmlFor="in">Location</label>
        </span>
      </div>
      <Button className="p-mt-4" label="Submit" />
    </div>
  );
}