// src/pages/Todo.js

import React, { useEffect, useState } from 'react';
import { DataGrid } from '@material-ui/data-grid';

// functional component yang menampilkan todo list dari sumber eksternal
export default function Todo() {

  // definisi dari hooks, yang menyimpan data dinamis
  const [todolist, setTodo] = useState([]);

  // mengaktifkan getData untuk binding dengan todolist atau hooks lainnya
  useEffect(() => {
    getData()
  }, [])

  // sumber eksternal data, tinggal ditampilkan
  async function getData() {
    try {
      // metode standar fetch
      const result = await fetch('https://jsonplaceholder.typicode.com/todos');
      const todo = await result.json();
      // setTodo ditaruh sini, karena disinilah si hooks akan mengalami perubahan data. Syntax ikutin aja
      setTodo(Object.values(todo));
    } catch (error) {
      console.log(error)
    }
  }

  const columns = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'title', headerName: 'Task', width: 130 },
    { field: 'completed', headerName: 'Completed?', width: 130 },
  ];
  
  function DataTable() {
    return (
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid rows={todolist} columns={columns} pageSize={10} checkboxSelection />
      </div>
    );
  }

  function filterTaskByUserId(list, userId) {
    return list.filter((x) => x.userId === userId)
  }

  // component diawali huruf besar
  function DisplayUsers() {
    const user6 = filterTaskByUserId(todolist, 6);
    const user8 = filterTaskByUserId(todolist, 8);
    // mejik untuk menggabungkan array ala ES6, recommended di React.
    // cara ini mempertahankan immutability dari array.
    const users = [...user6,...user8]; // sama dengan const users = user6.concat(user8)
    return (
      <div>
        <h2>Task User 6 dan 8</h2>
        {users.map((x) => 
        <p>User {x.userId} kerjakan {x.title}</p>
        )}
      </div>
    )
  }

  return(
    <div>
      {/* melakukan perulangan untuk data todolist */}
      {/* diawali dengan kurung kurawal, terus ada div, di dalamnya ada kurung kurawal lagi untuk variabel yang ditampilkan */}
      <DataTable />
      {/* menampilkan data task dari user id 6 dan 11*/}
      <DisplayUsers />
    </div>
  );
}