// App.js
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './App.css';
import 'primereact/resources/themes/vela-orange/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import About from './pages/About';
import CreatePlayer from "./pages/CreatePlayer";
import Home from './pages/Home';
import Todo from './pages/Todo';
import { Menubar } from 'primereact/menubar';

function App() {
  const items = [
    {label : 'Home', icons : 'pi pi-home'},
    {label : 'Players', icons : 'pi pi-user'}
  ];

  return (
    <div className="App">
      <Router>
      <div>
        <Menubar model={items} />
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/todo">
            <Todo />
          </Route>
          <Route path="/about">
            {/* name = abc adalah props */}
            <About name="abc"/>
          </Route>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/create" exact>
            <CreatePlayer />
          </Route>
        </Switch>
      </div>
    </Router>
    </div>
  );
}

export default App;
