// src/components/CustomButton.js

import React, { useState } from 'react';

export default function Button() {
  // definisi hooks
  const [buttonText, setButtonText] = useState("click me, please"); //definisi state 'buttonText', dengan nilai default 'click me, please'
  const [clickCount, addClickCount] = useState(0); //definisi state 'clickCount', dengan nilai default 0.

  function updateButton() {
    setButtonText("You clicked me.");
    addClickCount(clickCount + 1);
  }

  return (
    <button onClick={() => updateButton()}>
      {`${buttonText} ${clickCount} times`}
    </button>
  )
}